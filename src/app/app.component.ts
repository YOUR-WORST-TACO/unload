import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'unload';
  todoArray=[];

  addTodo(value) {
    this.todoArray.push(value);
    console.log(this.todoArray);
  }
}
